# Cryogen Now!

Because friends don't let friends use Jekyll. Or Github...

## To set up your Cryogen site:

1. Fork this project.
2. In your new project settings, rename it and `Remove fork relationship`. Make sure to edit the project URL in the `Advanced` section.
3. Edit `resources/templates/config.edn` to configure site with your name, etc.
4. `resources/templates/themes/blue/html/base.html` is your main template where you can change your links and stuff. You can select a different theme in `config.edn`.
5. Edit the files in `resources/templates/md/posts` to make your posts!

Your site is rebuilt every time you edit a file and it's really fun to watch. View the console activity by clicking on the progress icon.